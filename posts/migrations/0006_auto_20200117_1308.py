# Generated by Django 3.0.1 on 2020-01-17 13:08

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0005_auto_20200117_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 17, 13, 8, 19, 211153, tzinfo=utc)),
        ),
    ]
