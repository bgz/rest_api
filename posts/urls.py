from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()
router.register('users', UserViewSet, basename='users')
router.register('comments', CommentViewSet, basename='comment')
router.register('', PostViewSet, basename='posts')


urlpatterns = router.urls
#     [
#     # path('users/', UserList.as_view()),
#     # path('users/<int:pk>/', UserDetail.as_view()),
#     # path('<int:pk>/', PostDetail.as_view(), name='detail'),
#     # path('', PostList.as_view(), name='list'),
#     # path('comment/', CommentList.as_view(), name='comment_list'),
#     # path('comment/create/', CommentCreateView.as_view(), name='comment_create'),
#     # path('create/', PostCreate.as_view()),
#     # path('<int:pk>/update/', PostUpdate.as_view()),
#     # path('<int:pk>/delete/', PostDelete.as_view()),
# ]